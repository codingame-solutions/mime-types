//https://www.codingame.com/ide/puzzle/mime-type

const N = parseInt(readline()); // Number of elements which make up the association table.
const Q = parseInt(readline()); // Number Q of file names to be analyzed.

let mimes = [];
let filesToAnalyse = [];
let extToAnalyse = [];

for (let i = 0; i < N; i++) {
    var inputs = readline().split(' ');
    const EXT = inputs[0]; // file extension
    const MT = inputs[1]; // MIME type.
    // console.error("EXT IS " + EXT);
    // console.error("MT IS " + MT);
    mimes[EXT.split('.')[0].toLowerCase()] = MT;
}
for (let i = 0; i < Q; i++) {
    const FNAME = readline(); // One file name per line.
    let hasNoExt = FNAME.lastIndexOf('.') < 0;
    let lastNamePart = FNAME.split('.')[FNAME.lastIndexOf('.') - 1];
    // let lastNamePart = FNAME.slice(0, FNAME.lastIndexOf('.'))
    // console.error("HERE " + FNAME.slice(0, FNAME.lastIndexOf(lastNamePart) + 1 ));
    // let fullNamePart = FNAME.slice(0, FNAME.lastIndexOf(lastNamePart) + 1 );
    let hasNoName = lastNamePart < 0;
    // let hasNoName = lastNamePart < 0 || fullNamePart.length === 0;
    const filenameExt = (hasNoExt || hasNoName) ? "UNKNOWN" : FNAME.split('.').pop().toLowerCase();
    filesToAnalyse[FNAME] = filenameExt;
    // console.error(filenameExt);
}

for ( var fname in filesToAnalyse ) {
  console.log((mimes[filesToAnalyse[fname]] || "UNKNOWN"));
}
